<?php

/**
 * @file
 * Contains Drupal_SearchApi_Facetapi_QueryType_DateCalendarQueryType.
 */

/**
 * Date calendar query type plugin for the Apache Solr Search Integration adapter.
 */
class Drupal_SearchApi_Facetapi_QueryType_DateCalendarQueryType extends SearchApiFacetapiDate implements FacetapiQueryTypeInterface {

  /**
   * Implements FacetapiQueryTypeInterface::getType().
   */
  static public function getType() {
    return 'date_calendar';
  }

  /**
   * Implements FacetapiQueryTypeInterface::execute().
   */
  public function execute($query) {
    $this->adapter->addFacet($this->facet, $query);
    dsm($this->adapter->getActiveItems($this->facet));
    dsm($query);
    dsm($this->build());
    if ($active = $this->adapter->getActiveItems($this->facet)) {
      dsm($active);
      // Check the first value since only one is allowed.
      $filter = self::mapFacetItemToFilter(key($active));
      if ($filter) {
        $this->addFacetFilter($query, $this->facet['field'], $filter);
      }
    }
  }

  /**
   * Implements FacetapiQueryTypeInterface::build().
   *
   * Unlike normal facets, we provide a static list of options.
   */
  public function build() {
    $facet = $this->adapter->getFacet($this->facet);
    $search_ids = drupal_static('search_api_facetapi_active_facets', array());

    if (empty($search_ids[$facet['name']]) || !search_api_current_search($search_ids[$facet['name']])) {
      return array();
    }
    $search_id = $search_ids[$facet['name']];

    $build = array();
    $search = search_api_current_search($search_id);
    $results = $search[1];
    if (!$results['result count']) {
      return array();
    }

    // Executes query, iterates over results.
    if (isset($results['search_api_facets']) && isset($results['search_api_facets'][$this->facet['field']])) {
      $values = $results['search_api_facets'][$this->facet['field']];
      //$build = date_facet_calendar_get_ranges($values);
      $now = $_SERVER['REQUEST_TIME'];
      // Calculate values by facet.
      foreach ($values as $value) {
        $value['filter'] = str_replace('"', '', $value['filter']);
        $timestamp =  str_replace('"','',$value['filter']);
        $timestamp = (int) $timestamp;

        // TODO: make start / end configurable
        $timestamp_start = strtotime("0:00",$timestamp);
        $timestamp_end = strtotime("23:59",$timestamp);
        $interval = '['.$timestamp_start .' TO '. $timestamp_end.']';
        
        $date  = format_date($timestamp, 'day_only');
      
        $count =$new_build[$value['filter']] +  $value['count'];
        $new_build[$timestamp_start] = array(
          '#markup' => $date,
          '#time_interval' => $interval,
          '#count' => $count,
        );

        /*
        $diff = $now - $value['filter'];
        foreach ($build as $key => $item) {
          if ($diff < $item['#time_interval']) {
            $build[$key]['#count'] += $value['count'];
          }
        }
         */
      }
    }
    /*
    dsm('build');
    dsm($build);
    dsm('new_build');
    dsm($new_build);
     */


    // Unset empty items.
    /*
    foreach ($build as $key => $item) {
      if ($item['#count'] === NULL) {
        unset($build[$key]);
      }
    }
     */
    // TODO: push to settings as json array
    drupal_add_js('', 'settings');

    // Gets total number of documents matched in search.
    $total = $results['result count'];
    $keys_of_active_facets = array();
    // Gets active facets, starts building hierarchy.
    foreach ($this->adapter->getActiveItems($this->facet) as $key => $item) {
      // If the item is active, the count is the result set count.
      $new_build[$key]['#count'] = $total;
      $keys_of_active_facets[] = $key;
    }

    // If we have active item, unset other items.
    if (!empty($keys_of_active_facets)) {
      foreach ($new_build as $key => $item) {
        if (!in_array($key, $keys_of_active_facets)) {
          unset($new_build[$key]);
        }
      }
    }
    return $new_build;
  }

  /**
   * Maps a facet item to a filter.
   *
   * @param string $key
   *   Facet item key, for example 'past_hour'.
   *
   * @return string|false
   *   A string that can be used as a filter, false if no filter was found.
   */
  public function mapFacetItemToFilter($key) {
    dsm('mapFacetItemToFilter');
    $options = self::getFacetItems();
    return isset($options[$key]) ? $options[$key] : FALSE;
  }

  /**
   * Gets a list of facet items and matching filters.
   *
   * @return array
   *   List of facet items and their filters.
   */
  public function getFacetItems() {
    $now = $_SERVER['REQUEST_TIME'];
    $past_hour = strtotime('-1 hour');
    $past_24_hours = strtotime('-24 hour');
    $past_week = strtotime('-1 week');
    $past_month = strtotime('-1 month');
    $past_year = strtotime('-1 year');

    $options = array(
      'past_hour'     => "[$past_hour TO $now]",
      'past_24_hours' => "[$past_24_hours TO $now]",
      'past_week'     => "[$past_week TO $now]",
      'past_month'    => "[$past_month TO $now]",
      'past_year'     => "[$past_year TO $now]",
    );

    return $options;
  }
}
