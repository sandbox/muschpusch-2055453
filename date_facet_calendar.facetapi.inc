<?php

/**
 * @file
 * Facet API hook implementations.
 */

/**
 * Implements hook_facetapi_widgets().
 */
function date_facet_calendar_facetapi_widgets() {
  return array(
    'date_calendar' => array(
      'handler' => array(
        'label' => t('Calendar'),
        'class' => 'Drupal_Apachesolr_Facetapi_Widget_DateCalendarWidget',
        'query types' => array('date_calendar'),
      ),
    ),
  );
}

/**
 * Implements hook_facetapi_query_types().
 */
function date_facet_calendar_facetapi_query_types() {
  return array(
    'apachesolr_date_calendar' => array(
      'handler' => array(
        'class' => 'Drupal_Apachesolr_Facetapi_QueryType_DateCalendarQueryType',
        'adapter' => 'apachesolr',
      ),
    ),
    'search_date_calendar' => array(
      'handler' => array(
        'class' => 'Drupal_Search_Facetapi_QueryType_DateCalenderQueryType',
        'adapter' => 'search',
      ),
    ),
    'search_api_date_calendar' => array(
      'handler' => array(
        'class' => 'Drupal_SearchApi_Facetapi_QueryType_DateCalendarQueryType',
        'adapter' => 'search_api',
      ),
    )
  );
}
